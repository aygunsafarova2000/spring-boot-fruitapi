package com.tech.lesson4.rest;

import com.tech.lesson4.dto.FruitRequestDto;
import com.tech.lesson4.dto.FruitResponseDto;
import com.tech.lesson4.service.FruitService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/fruits")
public class FruitApi {

    private  final FruitService fruitService;

    public FruitApi(FruitService fruitService) {
        this.fruitService = fruitService;
    }

    @GetMapping
    public List<FruitResponseDto> getFruitList(@RequestParam(value="from", required = false) Integer from,
                                              @RequestParam(value="to",required = false) Integer to)
    {

        return fruitService.list(from,to);
    }


    @GetMapping("/{id}")
    public FruitResponseDto getFruitById(@PathVariable Long id){
        return fruitService.get(id);
    }


    @PostMapping
    public FruitResponseDto addFruit(@Validated @RequestBody FruitRequestDto fruitRequestDto){

        return fruitService.post(fruitRequestDto);
    }


    @PutMapping("/{id}")
    public FruitResponseDto updateFruit(@PathVariable Long id,
                                       @Validated @RequestBody FruitRequestDto fruitRequestDto){

       return fruitService.update(id, fruitRequestDto);
    }

    @DeleteMapping("/{id}")

    public void deleteFruit(@PathVariable Long id){
        fruitService.delete(id);
    }







}
