package com.tech.lesson4.repository;

import com.tech.lesson4.domain.FruitEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FruitRepository extends JpaRepository<FruitEntity,Long> {



}
