package com.tech.lesson4.service;

import com.tech.lesson4.dto.FruitRequestDto;
import com.tech.lesson4.dto.FruitResponseDto;

import java.util.List;

public interface FruitService {

     List<FruitResponseDto> list(Integer from, Integer to);

     FruitResponseDto get(Long id);
     FruitResponseDto post(FruitRequestDto fruitRequestDto);
     FruitResponseDto update(Long id, FruitRequestDto fruitRequestDto);

     void delete(Long id);
}
