package com.tech.lesson4.service;

import com.tech.lesson4.domain.FruitEntity;
import com.tech.lesson4.dto.FruitRequestDto;
import com.tech.lesson4.dto.FruitResponseDto;
import com.tech.lesson4.repository.FruitRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FruitServiceImpl implements  FruitService{

    private final FruitRepository fruitRepository;

    @Override
    public List<FruitResponseDto> list(Integer from, Integer to) {

        return   fruitRepository.findAll().stream().
                map(fruitEntity -> FruitResponseDto.
                        builder().
                        price(fruitEntity.getPrice()).
                        name(fruitEntity.getName()).
                        amount(fruitEntity.getAmount()).
                        id(fruitEntity.getId())
                        .build()).
                toList();
    }

    @Override
    public FruitResponseDto get(Long id) {

        Optional<FruitEntity> fruitEntityOptional = fruitRepository.findById(id);

        if(fruitEntityOptional.isPresent()){
            FruitEntity fruitEntity=fruitEntityOptional.get();
            return FruitResponseDto.builder()
                    .price(fruitEntity.getPrice())
                    .name(fruitEntity.getName())
                    .id(fruitEntity.getId())
                    .amount(fruitEntity.getAmount())
                    .build();

        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Fruit with id " + id + " is not exist");

        }

    }

    @Override
    public FruitResponseDto post(FruitRequestDto fruitRequestDto) {

       FruitEntity fruitEntity= FruitEntity.builder()
                .amount(fruitRequestDto.getAmount()).
                price(fruitRequestDto.getPrice()).
                name(fruitRequestDto.getName()).
                build();
       FruitEntity fruitEntity1= fruitRepository.save(fruitEntity);
        return FruitResponseDto.
                builder().
                id(fruitEntity1.getId()).
                amount(fruitEntity1.getAmount()).
                name(fruitEntity1.getName()).
                price(fruitEntity1.getPrice()).
                build();
    }

    @Override
    public FruitResponseDto update(Long id, FruitRequestDto fruitRequestDto) {
        Optional<FruitEntity> fruitEntityOptional = fruitRepository.findById(id);

        if(fruitEntityOptional.isPresent()){

            FruitEntity oldFruit= fruitEntityOptional.get();
            oldFruit.setAmount(fruitRequestDto.getAmount());
            oldFruit.setName(fruitRequestDto.getName());
            oldFruit.setPrice(fruitRequestDto.getPrice());

           FruitEntity updatedFruit= fruitRepository.save(oldFruit);

            return FruitResponseDto.builder()
                    .id(updatedFruit.getId())
                    .name(updatedFruit.getName())
                    .amount(updatedFruit.getAmount())
                    .price(updatedFruit.getPrice())
                    .build();

        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Fruit with id " + id + " is not exist");
        }
    }

    @Override
    public void delete(Long id) {

        Optional<FruitEntity> fruitEntityOptional = fruitRepository.findById(id);

        if(fruitEntityOptional.isPresent()){
            FruitEntity fruitEntity = fruitEntityOptional.get();
            fruitRepository.delete(fruitEntity);
        }
        else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Fruit with id " + id + " is not exist");
        }

    }
}
