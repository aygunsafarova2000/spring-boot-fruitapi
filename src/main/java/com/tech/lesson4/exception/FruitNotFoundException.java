package com.tech.lesson4.exception;

public class FruitNotFoundException extends RuntimeException{

    public FruitNotFoundException(String message) {
        super(message);
    }
}
