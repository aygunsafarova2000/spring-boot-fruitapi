package com.tech.lesson4.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FruitResponseDto {

    private Long id;
    private String name;
    private String amount;
    private Double price;

}
